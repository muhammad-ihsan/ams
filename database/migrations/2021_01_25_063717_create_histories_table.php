<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_id');
            $table->integer('category_id');
            $table->integer('location_id');
            $table->integer('employee_id')->nullable();
            $table->integer('partner_id')->nullable();
            $table->string('name');
            $table->string('code');
            $table->string('reference');
            $table->enum('status', ['stock', 'use', 'not use'])->nullable()->default('stock');
            $table->date('acquisitioned_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
