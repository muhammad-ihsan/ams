<!DOCTYPE html>
<html lang="en">
    <head>
        @component('partials.head', ['pageTitle' => $pageTitle])
        @endcomponent
    </head>
    <body>
        <div id="app">
            <div class="main-wrapper main-wrapper main-wrapper-1">
                <div class="navbar-bg"></div>
                @component('partials.header')@endcomponent
                <div class="main-sidebar sidebar-style-2">
                    @component('partials.sidebar', ['pageActive' => $pageActive])
                    @endcomponent
                </div>
                <!-- Main Content -->
                <div class="main-content">
                    <section class="section">
                        <div class="section-header">
                            <h1>{{ $pageTitle }}</h1>
                            @if(isset($breadcrumbs))
                            <div class="section-header-breadcrumb">
                                @foreach($breadcrumbs as $breadcrumb)
                                <div class="breadcrumb-item active">
                                    <a href="{{ url($breadcrumb['path']) }}">{{ $breadcrumb['name'] }}</a>
                                </div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                        <div class="section-body">
                            @yield('content')
                        </div>
                    </section>  
                </div>
            </div>
        </div>
        <!-- General JS Scripts -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="{{ ('assets/js/stisla.js') }}"></script>
        <!-- JS Libraies -->
        <!-- Template JS File -->
        <script src="{{ ('assets/js/scripts.js') }}"></script>
        <script src="{{ ('assets/js/custom.js') }}"></script>
        <!-- Page Specific JS File -->
    </body>
</html>
