<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="#">Assets Management</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="#">AM</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">MAIN MENU</li>
        <li class="nav-item {{ $pageActive == 'dashboard' ? 'active' : '' }}">
            <a href="{{ url('dashboard') }}" class="nav-link">
                <i class="fas fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item {{ $pageActive == 'asset' ? 'active' : '' }}">
            <a href="{{ url('asset') }}" class="nav-link">
                <i class="fas fa-cube"></i>
                <span>Asset</span>
            </a>
        </li>
        <li class="nav-item {{ $pageActive == 'category' ? 'active' : '' }}">
            <a href="{{ url('category') }}" class="nav-link">
                <i class="fas fa-tags"></i>
                <span>Kategori</span>
            </a>
        </li>
        <li class="nav-item {{ $pageActive == 'location' ? 'active' : '' }}">
            <a href="{{ url('location') }}" class="nav-link">
                <i class="fas fa-building"></i>
                <span>Lokasi</span>
            </a>
        </li>
        <li class="nav-item {{ $pageActive == 'report' ? 'active' : '' }}">
            <a href="{{ url('report') }}" class="nav-link">
                <i class="fas fa-clipboard"></i>
                <span>Laporan</span>
            </a>
        </li>
        <li class="nav-item {{ $pageActive == 'employee' ? 'active' : '' }}">
            <a href="{{ url('employee') }}" class="nav-link">
                <i class="fas fa-users"></i>
                <span>Karyawan</span>
            </a>
        </li>
        <li class="nav-item {{ $pageActive == 'partner' ? 'active' : '' }}">
            <a href="{{ url('partner') }}" class="nav-link">
                <i class="fas fa-comments"></i>
                <span>Mitra</span>
            </a>
        </li>
        

        <li class="menu-header">AKUN SAYA</li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="fas fa-user"></i>
                <span>Ubah Profile</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="fas fa-lock"></i>
                <span>Ubah Password</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="fas fa-sign-out-alt"></i>
                <span>Keluar</span>
            </a>
        </li>
    </ul>
</aside>