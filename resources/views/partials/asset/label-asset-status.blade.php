@if($status == 'use')
<div class="badge badge-success">Digunakan</div>
@endif

@if($status == 'not use')
<div class="badge badge-danger">Tidak Digunakan</div>
@endif

@if($status == 'stock')
<div class="badge badge-info">Stok</div>
@endif