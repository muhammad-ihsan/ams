@extends('layouts.default')


@section('content')
<div class="card">
	<div class="card-body text-right">
		<a href="{{ url('asset/add') }}" class="btn btn-primary">
			<i class="fa fa-plus-circle"></i> BUAT BARU
		</a>
	</div>
</div>

<div class="card">
	<div class="card-body p-0">
		<table class="table">
			<thead>
				<tr>
					<th class="w-50">Asset</th>
					<th class="w-50">Lokasi</th>
					<th class="w-50">Status</th>
				</tr>
			</thead>
			<tbody>
				@foreach($assets as $asset)
				<tr>
					<td>
						{{ $asset->code }} <br>
						<small class="text-muted">{{ $asset->name }}</small>
						<div class="table-links">
							<a href="{{ url('asset/detail/'.$asset->id) }}">View</a>
						</div>
					</td>
					<td>{{ $asset->location->name }}</td>
					<td>@component('partials.asset.label-asset-status', ['status' => $asset->status]) @endcomponent</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection