<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Assets extends Controller
{
    public function index()
    {
    	$data = [
			'pageTitle'  => 'Daftar Asset',
			'pageActive' => 'asset'
    	];


        $assets         = \App\Asset::get();
        $data['assets'] = $assets;
    	return view('asset.index', $data);
    }

    public function add()
    {
        $data = [
            'pageTitle'  => 'Buat Baru',
            'pageActive' => 'asset'
        ];
        return view('asset.add', $data);
    }

    public function detail($assetId)
    {
        $data = [
            'pageTitle'  => 'Daftar Asset',
            'pageActive' => 'asset'
        ];

        $asset          = \App\Asset::find($assetId);
        return view('asset.detail', $data);
    }
}
