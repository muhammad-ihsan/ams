<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Dashboard extends Controller
{
    public function index()
    {
    	$data = [
    		'pageTitle' 	=> 'Dashboard',
    		'pageActive'	=> 'dashboard'
    	];


    	return view('dashboard', $data);
    }
}
